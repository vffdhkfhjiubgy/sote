#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Config.hpp>
#include <fstream>

using namespace sf;

int main() {
	//starting window
	RenderWindow SOTE(VideoMode(1280, 720, 32), "Story of the Elbow v0.0.1 ALPHA"/*, Style::Fullscreen*/);

	//setting textures

	//Front
	Texture player_front;
	player_front.loadFromFile("textures/player_front.png");

	//Back
	Texture player_back;
	player_back.loadFromFile("textures/player_back.png");

	//Left
	Texture player_left;
	player_left.loadFromFile("textures/player_left.png");

	//Right
	Texture player_right;
	player_right.loadFromFile("textures/player_right.png");

	//default player texture
	Sprite player;
	player.setTexture(player_front);

	//main game loop
	for (;;) {

		SOTE.clear(Color(0, 0, 0));

		if (Keyboard::isKeyPressed(Keyboard::A)) {
			player.setTexture(player_left);
		}
		else if (Keyboard::isKeyPressed(Keyboard::D)) {
			player.setTexture(player_right);
		}
		else if (Keyboard::isKeyPressed(Keyboard::W)) {
			player.setTexture(player_back);
		}
		else if (Keyboard::isKeyPressed(Keyboard::S)) {
			player.setTexture(player_front);
		}
		else {
			player.setTexture(player_front);
		}
		SOTE.draw(player);
		SOTE.display();
	}
	return 0;
}